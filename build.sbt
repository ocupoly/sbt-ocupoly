import ReleaseTransformations._

lazy val root = (project in file("."))
  .enablePlugins(SbtPlugin, ReleasePlugin, ScalafmtPlugin, GitVersioning, GitPlugin)
  .settings(
    name := "sbt-ocupoly",
    organization := "com.ocupoly",
    organizationName := "Ocupoly",
    versionScheme := Some("semver-spec"),
    scalacOptions ++= Seq(
      "-encoding",
      "UTF-8",
      "-deprecation",
      "-unchecked"
    ),
    scalafmtCheck := true,
    scalafmtSbtCheck := true,
    libraryDependencies ++= Seq(
      "org.slf4j" % "slf4j-nop" % "1.7.25"
    ),
    addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.5.4"),
    addSbtPlugin("com.github.sbt" % "sbt-release" % "1.4.0"),
    addSbtPlugin("com.github.mwegrz" % "sbt-logback" % "0.1.8"),
    addSbtPlugin("com.github.sbt" % "sbt-git" % "2.1.0"),
    addSbtPlugin("com.github.sbt" % "sbt-native-packager" % "1.11.1"),
    addSbtPlugin("io.spray" % "sbt-revolver" % "0.10.0"),
    addSbtPlugin("com.github.sbt" % "sbt-pgp" % "2.3.1"),
    addSbtPlugin("org.xerial.sbt" % "sbt-sonatype" % "3.12.2"),
    addSbtPlugin("com.github.sbt" % "sbt-protobuf" % "0.8.2"),
    // Release settings
    releaseTagName := { (ThisBuild / version).value },
    releaseTagComment := s"Release version ${(ThisBuild / version).value}",
    releaseCommitMessage := s"Set version to ${(ThisBuild / version).value}",
    releaseProcess := Seq[ReleaseStep](
      checkSnapshotDependencies,
      inquireVersions,
      runClean,
      releaseStepCommandAndRemaining("+test"),
      setReleaseVersion,
      commitReleaseVersion,
      tagRelease,
      setNextVersion,
      commitNextVersion,
      pushChanges
    ),
    // Publish settings
    resolvers ++= Resolver.sonatypeOssRepos("snapshots"),
    Test / publishArtifact := false,
    pomIncludeRepository := { _ =>
      false
    },
    homepage := Some(url("http://gitlab.com/ocupoly/sbt-ocupoly")),
    scmInfo := Some(
      ScmInfo(
        url("https://gitlab.com/ocupoly/sbt-ocupoly.git"),
        "scm:git@gitlab.com:ocupoly/sbt-ocupoly.git"
      )
    ),
    developers := List(
      Developer(
        id = "ocupoly",
        name = "Ocupoly",
        email = null,
        url = url("https://www.ocupoly.com")
      )
    ),
    git.useGitDescribe := true,
    DefaultOptions.addCredentials,
    resolvers ++= Seq(
      "Ocupoly GitLab" at "https://gitlab.com/api/v4/groups/8019089/-/packages/maven"
    ),
    publishTo := Some(
      "Ocupoly GitLab Publishing" at "https://gitlab.com/api/v4/projects/67084937/packages/maven"
    )
  )
