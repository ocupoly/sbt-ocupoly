package com.ocupoly.sbtocupoly

import sbt.Keys._
import sbt.{ Setting, _ }
import sbtrelease.ReleasePlugin.autoImport._
import ReleaseTransformations._
import com.jsuereth.sbtpgp.PgpKeys

import scala.language.implicitConversions

import scala.language.implicitConversions

object OcupolyApache2LibraryPlugin extends OcupolyLibraryPlugin {
  override def projectSettings: Seq[Setting[_]] =
    super.projectSettings ++ Seq(
      releaseProcess := Seq[ReleaseStep](
        checkSnapshotDependencies,
        inquireVersions,
        runClean,
        releaseStepCommandAndRemaining("test"),
        setReleaseVersion,
        commitReleaseVersion,
        tagRelease,
        releaseStepCommand("publishSigned"),
        setNextVersion,
        commitNextVersion,
        releaseStepCommand("sonatypeReleaseAll"),
        pushChanges
      ),
      releasePublishArtifactsAction := PgpKeys.publishSigned.value,
      licenses := Seq(
        "Apache License, Version 2.0" -> url("http://www.apache.org/licenses/LICENSE-2.0.html")
      ),
      homepage := None,
      scmInfo := None
    )
}
