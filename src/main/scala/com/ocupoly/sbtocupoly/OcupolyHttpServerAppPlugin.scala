package com.ocupoly.sbtocupoly

import com.typesafe.sbt.packager.archetypes.JavaServerAppPackaging
import com.typesafe.sbt.SbtNativePackager
import com.typesafe.sbt.SbtNativePackager.autoImport._
import com.typesafe.sbt.packager.docker.DockerPlugin
import com.typesafe.sbt.packager.docker.DockerPlugin.autoImport._
import sbt.Keys._
import sbt.{ Setting, _ }
import spray.revolver.RevolverPlugin
import spray.revolver.RevolverPlugin.autoImport._
import com.typesafe.sbt.packager.universal.UniversalPlugin.autoImport._
import OcupolyLibraryDependencies._

object OcupolyHttpServerAppPlugin extends OcupolyLibraryPlugin {
  override def requires: Plugins =
    super.requires && SbtNativePackager && JavaServerAppPackaging && DockerPlugin && RevolverPlugin

  override def projectSettings: Seq[Setting[_]] =
    super.projectSettings ++ Seq(
      libraryDependencies ++= Seq(
        AkkaActor,
        AkkaStream,
        AkkaStreamTestKit,
        AkkaPersistence,
        AkkaPersistenceQuery,
        AkkaCluster,
        AkkaClusterTools,
        AkkaPersistenceCassandra,
        AkkaPersistenceCassandraLauncher % "test",
        AkkaSlf4j,
        AkkaHttp,
        AkkaHttpCors,
        AkkaHttp2Support,
        AkkaHttpTestkit % "test",
        AkkaHttpCirce,
        AkkaHttpAvro4s,
        AkkaPersistenceTyped,
        AkkaStreamAlpakkaCassandra,
        // CassandraJavaDriverCore,
        // CassandraDriverExtras,
        NettyTransportNativeEpoll,
        JwtCirce,
        CirceCore,
        CirceGeneric,
        CirceGenericExtras,
        CirceParser,
        Avro4sCore,
        Courier,
        Magnolia
      ),
      dependencyOverrides ++= Seq(
        CirceCore,
        CirceGeneric,
        CirceGenericExtras,
        // CassandraDriverCore,
        // CassandraDriverExtras,
        "com.google.guava" % "guava" % "19.0"
      ),
      resolvers ++= Resolver.sonatypeOssRepos("snapshots"),
      Compile / packageDoc / publishArtifact := false,
      Revolver.enableDebugging(port = 5050, suspend = false),
      maintainer := "Ocupoly <support@ocupoly.com>",
      packageSummary := name.value,
      packageDescription := name.value,
      topLevelDirectory := None,
      dockerBaseImage := "registry.gitlab.com/ocupoly/docker-java-jre-bash:1.4.1",
      dockerUpdateLatest := false,
      dockerAlias := DockerAlias(
        dockerRepository.value,
        dockerUsername.value,
        (Docker / packageName).value,
        Some((Docker / version).value)
      ),
      dockerExposedPorts := Seq(8080),
      dockerRepository := Some("registry.gitlab.com"),
      dockerUsername := Some("ocupoly"),
      dockerBuildxPlatforms := Seq("linux/amd64", "linux/arm64/v8")
    )
}
