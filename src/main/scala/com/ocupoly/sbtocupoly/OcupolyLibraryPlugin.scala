package com.ocupoly.sbtocupoly

import com.github.mwegrz.sbtlogback.LogbackPlugin
import com.github.mwegrz.sbtlogback.LogbackPlugin.autoImport.*
import OcupolyLibraryDependencies.*
import sbt.Keys.*
import sbt.{ Setting, * }
import sbtrelease.ReleasePlugin
import sbtrelease.ReleasePlugin.autoImport.*
import ReleaseTransformations.*
import com.github.sbt.git.GitVersioning
import com.github.sbt.git.GitPlugin
import com.github.sbt.git.GitPlugin.autoImport.git
import org.scalafmt.sbt.ScalafmtPlugin.autoImport.*
import org.scalafmt.sbt.ScalafmtPlugin
import sbtprotobuf.ProtobufPlugin

object OcupolyLibraryPlugin extends OcupolyLibraryPlugin

trait OcupolyLibraryPlugin extends AutoPlugin {
  override def requires: Plugins =
    ScalafmtPlugin && LogbackPlugin && ReleasePlugin &&
      GitVersioning && GitPlugin && ProtobufPlugin

  override def trigger: PluginTrigger = noTrigger

  object autoImport {
    val gitlabBaseUri = settingKey[String](
      "GitLab base URI"
    )

    val gitlabGroupId = settingKey[Int](
      "GitLab group ID"
    )

    val gitlabProjectId = settingKey[Int](
      "GitLab project ID"
    )
  }
  import autoImport.*

  override def projectSettings: Seq[Setting[?]] =
    Seq(
      organization := "com.ocupoly",
      organizationName := "Ocupoly",
      versionScheme := Some("semver-spec"),
      developers := List(
        Developer(
          id = "ocupoly",
          name = "Ocupoly",
          email = "support@ocupoly.com",
          url = url("https://www.ocupoly.com")
        )
      ),
      scalaVersion := OcupolyLibraryDependencies.Versions.Scala,
      scalacOptions := Seq(),
      scalafmtCheck := true,
      scalafmtSbtCheck := true,
      slf4jVersion := OcupolyLibraryDependencies.Versions.Slf4j,
      logbackVersion := OcupolyLibraryDependencies.Versions.Logback,
      libraryDependencies ++= Seq(
        ThreetenExtra,
        ScalaTest % "test",
        ScalaCheck % "test",
        LogbackHocon,
        ScalaStructlog,
        Config
      ),
      DefaultOptions.addCredentials,
      gitlabBaseUri := "https://gitlab.com/api",
      gitlabGroupId := 8019089,
      resolvers ++= Seq(
        "Ocupoly GitLab" at s"${gitlabBaseUri.value}/v4/groups/${gitlabGroupId.value.toString}/-/packages/maven"
      ),
      publishTo := Some(
        "Ocupoly GitLab Publishing" at s"https://gitlab.com/api/v4/projects/${gitlabProjectId.value.toString}/packages/maven"
      ),
      releaseCrossBuild := false,
      releaseTagName := { (ThisBuild / version).value },
      releaseTagComment := s"Release version ${(ThisBuild / version).value}",
      releaseCommitMessage := s"Set version to ${(ThisBuild / version).value}",
      releaseProcess := Seq[ReleaseStep](
        checkSnapshotDependencies,
        inquireVersions,
        runClean,
        releaseStepCommandAndRemaining("+test"),
        setReleaseVersion,
        commitReleaseVersion,
        tagRelease,
        setNextVersion,
        commitNextVersion,
        pushChanges
      ),
      crossPaths := true,
      autoScalaLibrary := true,
      Test / publishArtifact := false,
      pomIncludeRepository := { _ => false },
      git.useGitDescribe := true,
      offline := false,
      run / fork := true,
      run / connectInput := true
    )
}
