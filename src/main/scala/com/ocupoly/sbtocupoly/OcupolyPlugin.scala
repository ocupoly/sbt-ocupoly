package com.ocupoly.sbtocupoly

import com.ocupoly.sbtocupoly
import sbt._

import scala.language.implicitConversions

object OcupolyPlugin extends AutoPlugin {
  object autoImport {
    lazy val OcupolyLibraryDependencies: sbtocupoly.OcupolyLibraryDependencies.type =
      sbtocupoly.OcupolyLibraryDependencies
  }
}
